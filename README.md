# tor-browser

Tor Browser Bundle: anonymous browsing using Firefox and Tor (international PKGBUILD)

https://www.torproject.org/projects/torbrowser.html

https://dist.torproject.org/torbrowser/

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/web-browsers/tor-browser.git
```

<br><br>

To add the tor gpg key, from terminal, without sudo:

```
gpg --auto-key-locate nodefault,wkd --locate-keys torbrowser@torproject.org
```
